<?php

/**
 * Package : Webworks
 * Module  : Sms
 * File    : Webworks/Sms/Model/Resource/Sms.php
 * Date    : 28-04-2018
 * Copyright : Copyright (c) 2017 Webworks.pk
 * @Author  : Naveed Asim <naveed.asim@webworks.pk>
 * @Company : Webworks Solutions Pvt. Ltd. <webworks.pk>
 */

namespace Webworks\Sms\Model\Resource;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Sms extends AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('webworks_sms', 'id');  // friends is name of table
    }
}