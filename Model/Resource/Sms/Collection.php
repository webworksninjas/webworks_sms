<?php

/**
 * Package : Webworks
 * Module  : Sms
 * File    : Webworks/Sms/Model/Resource/Collection.php
 * Date    : 28-04-2018
 * Copyright : Copyright (c) 2017 Webworks.pk
 * @Author  : Naveed Asim <naveed.asim@webworks.pk>
 * @Company : Webworks Solutions Pvt. Ltd. <webworks.pk>
 */

namespace Webworks\Sms\Model\Resource\Sms;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Define model & collection model
     */
    protected function _construct()
    {
        $this->_init(
            'Webworks\Sms\Model\Sms',
            'Webworks\Sms\Model\Resource\Sms'
        );
    }
}