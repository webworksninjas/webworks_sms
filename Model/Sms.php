<?php

/**
 * Package : Webworks
 * Module  : Sms
 * File    : Webworks/Sms/Model/Sms.php
 * Date    : 28-04-2018
 * Copyright : Copyright (c) 2017 Webworks.pk
 * @Author  : Naveed Asim <naveed.asim@webworks.pk>
 * @Company : Webworks Solutions Pvt. Ltd. <webworks.pk>
 */

namespace Webworks\Sms\Model;

use Magento\Framework\Model\AbstractModel;

class Sms extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Webworks\Sms\Model\Resource\Sms');
    }
}