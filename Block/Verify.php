<?php
/**
 * Package : Webworks
 * Module  : Sms
 * File    : Webworks/Sms/Block/Verify.php
 * Date    : 28-04-2018
 * Copyright : Copyright (c) 2017 Webworks.pk
 * @Author  : Naveed Asim <naveed.asim@webworks.pk>
 * @Company : Webworks Solutions Pvt. Ltd. <webworks.pk>
 */

namespace Webworks\Sms\Block;
use Magento\Sales\Model\Order;
 
class Verify extends \Magento\Framework\View\Element\Template
{
    
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Sales\Model\Order\Config
     */
    protected $_orderConfig;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $_httpContext;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */

    protected $_storeManager;

    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_checkoutSession = $checkoutSession;
        $this->_orderConfig = $orderConfig;
        $this->_isScopePrivate = true;
        $this->_storeManager = $storeManager;
        $this->_httpContext = $httpContext;
    }

    public function _getCurrentOrder() {
    	return $this->_checkoutSession->getLastRealOrder();
    }

	public function getSmsOrderId() {
        $order = $this->_getCurrentOrder();
        return $order->getEntityId();
	}

	public function getOrderStore()
	{
		$OrderStore = $this->_getCurrentOrder();
        $OrderStoreId = $OrderStore->getStoreId();

        $store = $this->_storeManager->getStore($OrderStoreId);

        return $store->getCode();
	}

	public function getOrderPaymentMethod() {
		$order = $this->_getCurrentOrder();
		return $order->getPayment()->getMethodInstance()->getCode();
	}

    public function getAjaxUrl(){
	    return $this->getUrl("sms/index/verify"); // Controller Url
	}
}