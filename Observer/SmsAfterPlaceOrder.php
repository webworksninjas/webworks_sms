<?php

/**
 * Package : Webworks
 * Module  : Sms
 * File    : Webworks/Sms/Observer/SmsAfterPlaceOrder.php
 * Date    : 28-04-2018
 * Copyright : Copyright (c) 2017 Webworks.pk
 * @Author  : Naveed Asim <naveed.asim@webworks.pk>
 * @Company : Webworks Solutions Pvt. Ltd. <webworks.pk>
 */

namespace Webworks\Sms\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

use Webworks\Sms\Model\SmsFactory;

class SmsAfterPlaceOrder implements ObserverInterface
{
    protected $_order;

    protected $_scopeConfig;

    protected $_modelSmsFactory;

    protected $_storeManager;

    /* GET Configuration values */

    const CONFIG_SMS_API_URL = 'itssms/sms/api_url';

    const CONFIG_SMS_API_KEY = 'itssms/sms/api_key';

    const CONFIG_SMS_MASK = 'itssms/sms/api_sender';

    const CONFIG_SMS_RESPONSE_TYPE = 'itssms/sms/api_response_type';

    const CONFIG_SMS_URL_PRAM = 'itssms/sms/api_url_parameters';

    const CONFIG_SMS_MSG_DATA = 'itssms/sms/api_msgdata';



    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        SmsFactory $modelSmsFactory
    ) {
         $this->_order = $order;
         $this->_scopeConfig = $scopeConfig;
         $this->_log = $logger;
         $this->_storeManager = $storeManager;
         $this->_modelSmsFactory = $modelSmsFactory;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
      
        $orderids = $observer->getEvent()->getOrderIds();


        foreach($orderids as $orderid){
            $order = $this->_order->load($orderid);
            $payment_method_code = $order->getPayment()->getMethodInstance()->getCode();
            $orderStore = $order->getStoreId();

            $store = $this->_storeManager->getStore($orderStore);


            if($payment_method_code == "cashondelivery" && ($store->getCode() == 'pk' || $store->getCode() == 'default'))
            {
                
                $telephone = $order->getShippingAddress()->getTelephone();

                $smsdModel = $this->_modelSmsFactory->create();
                $verificationCode = rand(3333, 9999);            

                if(strlen($telephone) > 9) {
                    $trim = strlen($telephone)-10;
                    $telephone = "92".substr($telephone,$trim);
                } else {
                    $telephone = "92".$telephone;
                }

                $smsdModel->setOrderId($order->getId());
                $smsdModel->setIncrementId($order->getIncrementId());
                $smsdModel->setVerificationCode($verificationCode);
                $smsdModel->setCustomerTelephone($telephone);
                $smsdModel->save();

                $this->sendSms($verificationCode,$telephone);
            }
            
            //echo $order_id = $order->getIncrementId();
        }
        //print_r($orderids);
        /**/

                
    }

    public function sendSms($vCode,$tel)
    {
        $apiUrl = $this->_scopeConfig->getValue(self::CONFIG_SMS_API_URL,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $apiKey = $this->_scopeConfig->getValue(self::CONFIG_SMS_API_KEY,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $apiMask = $this->_scopeConfig->getValue(self::CONFIG_SMS_MASK,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $responseType = $this->_scopeConfig->getValue(self::CONFIG_SMS_RESPONSE_TYPE,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $parameters = $this->_scopeConfig->getValue(self::CONFIG_SMS_URL_PRAM,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
        $msgData = $this->_scopeConfig->getValue(self::CONFIG_SMS_MSG_DATA,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        
        $smsMessage = str_replace('[VERIFICATION-CODE]', $vCode, $msgData);
        $pram = str_replace(',','&',$parameters);
        $arraySearch = array("[KEY]", "[RECEIVER]", "[SENDER]","[MESSAGE]","[RESPONSE]");
        $arrayReplace = array($apiKey,$tel,$apiMask,urlencode($smsMessage),$responseType);

        $urlPrameters = str_replace($arraySearch, $arrayReplace, $pram);

        try {
            
            $url = $apiUrl."?".$urlPrameters;

            //Curl Start
            $ch  =  curl_init();
            $timeout  =  30;
            curl_setopt ($ch,CURLOPT_URL, $url) ;
            curl_setopt ($ch,CURLOPT_RETURNTRANSFER, 1);
            curl_setopt ($ch,CURLOPT_CONNECTTIMEOUT, $timeout) ;
            $response = curl_exec($ch) ;
            curl_close($ch); 
            
        } catch (\Exception $ex) {
                $this->_log->pushHandler( new \Monolog\Handler\StreamHandler(BP.'/var/log/sms.log'));
                $this->_log->info($ex->getMessage());
        }
    }

}