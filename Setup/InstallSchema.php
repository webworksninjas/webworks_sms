<?php
/**
 * Package : Webworks
 * Module  : Sms
 * File    : Webworks/Sms/Observer/SmsAfterPlaceOrder.php
 * Date    : 28-04-2018
 * Copyright : Copyright (c) 2017 Webworks.pk
 * @Author  : Naveed Asim <naveed.asim@webworks.pk>
 * @Company : Webworks Solutions Pvt. Ltd. <webworks.pk>
 */

/**
* Install webworks_sms schema
*/

namespace Webworks\Sms\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
    * {@inheritdoc}
    * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
    */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
          /**
          * Create table 'webworks_sms'
          */
          $table = $setup->getConnection()
              ->newTable($setup->getTable('webworks_sms'))
              ->addColumn(
                  'id',
                  \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                  null,
                  ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                  'SMS ID'
              )
              ->addColumn(
                  'order_id',
                  \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                  null,
                  ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                  'Order Increment Id'
              )
              ->addColumn(
                  'increment_id',
                  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                  32,
                  [],
                  'Order Increment Id'
              )
              ->addColumn(
                  'customer_telephone',
                  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                  32,
                  [],
                  'Customer Mobile'
              )
              ->addColumn(
                  'verification_code',
                  \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                  null,
                  ['unsigned' => true, 'nullable' => true, 'default' => '0'],
                    'SMS verification code'
              )->setComment("Order verification sms");
          $setup->getConnection()->createTable($table);
      }
}