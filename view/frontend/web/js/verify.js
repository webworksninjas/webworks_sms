define([
        'jquery',
        ],
    function($,verify){

    return {
        verifyData:function(ajaxurl){
        $(document).on('click','.verify-sms',function (event){
                event.preventDefault();
                var data=$('.order-verification').serialize();
                $(".verification-notice p").css("display","none");
                //$(".checkout-success").css("display","none");
                //alert(data);
                $.ajax({
                    url:ajaxurl,
                    type:'POST',
                    showLoader: true,
                    dataType:'json',
                    data: { sms : data},                                      
                    success:function(response){
                        if(response == 1) {
                             $("#thankyou").css("display","block");
                             $(".checkout-success").css("display","none");
                            $(".verification-notice .code-success").css("display","block");
                            $(".sms-verification-form").css("display","none");
                            $(".sms_verify #block-sms-heading").css("display","none");
                            $(".checkout-success").css("display","block");
                        }else if(response == 0) {
                            $(".verification-notice .code-error").css("display","block");
                        }else {
                           $(".verification-notice .code-invalid").css("display","block"); 
                        }
                    }
                });
            });
        }
    }

});