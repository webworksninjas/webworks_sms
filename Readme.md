Webworks SMS:
====================
SMS Module to send out sms for all those orders placed using COD payment method and from PK store. Once order is placed SMS, with random verification code, will sent to customer mobile number and on success page she has to enter that verification code to verify the order.


Installation :

Step - 1:

1 - Create Webworks/Sms directory in app/code folder. ( if code folder is not presenet create that folder )

2 - Copy all files / folder in to Webworks/Sms folder.

3 - On command line ( your project root folder ) ru following commands :
	i   - $ php bin/magento setup:upgrade
	ii  - $ php bin/magento setup:di:compile


4 - Login to admin panel and go to Store->Configurations->Services->SMS Configuration

5 - Enter your Gateway settings ( i.e API Url, API key, Sender Mask, Response type, URL Parameters , Verification Message ( which sent out to customer ), SMS reply message ( after successfully entering the code ).


6 - Login to your admin panel and go to Store->Settings->Order Status

7 - Create Order status with Label "SMS Verified" , code "sms_verified" and assign 'pending' state to this status.

Requirements :
Magento 2.x

Compatible with : Magento 2.0, 2.1.x upto 2.2.3
