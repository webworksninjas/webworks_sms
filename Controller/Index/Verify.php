<?php

/**
 * Package : Webworks
 * Module  : Sms
 * File    : Webworks/Sms/Controller/Verify.php
 * Date    : 28-04-2018
 * Copyright : Copyright (c) 2017 Webworks.pk
 * @Author  : Naveed Asim <naveed.asim@webworks.pk>
 * @Company : Webworks Solutions Pvt. Ltd. <webworks.pk>
 */
 
namespace Webworks\Sms\Controller\Index;
 
use Magento\Framework\App\Action\Context;

use Webworks\Sms\Model\SmsFactory;

use Magento\Framework\Controller\ResultFactory;


class Verify extends \Magento\Framework\App\Action\Action
{
   // protected $_resultPageFactory;

    protected $_modelSmsFactory;

    protected $_modelOrder;
 
    public function __construct(Context $context,
        SmsFactory $modelSmsFactory,
        \Magento\Sales\Model\Order $order)
    {
        $this->_modelOrder = $order;

        parent::__construct($context);

        $this->_modelSmsFactory = $modelSmsFactory;
    }
 
    public function execute()
    {
        
        $response = $this->getRequest()->getPost('sms');
        parse_str($response,$parameter);


        $vCode = trim($parameter['verification_code']);
        $orderId = $parameter['id'];

        if(is_numeric($vCode) and is_numeric($orderId))
        {
            if(is_numeric($orderId)) {
                $smsdModel = $this->_modelSmsFactory->create();
                $OrderNumber = $smsdModel->load($orderId,'order_id');
                if($OrderNumber->getVerificationCode() == $vCode) {
                    $verifyOrder = $this->_modelOrder->load($orderId);
                    //$verifyOrder->setState('pending')->setStatus('sms_verified');
                    $verifyOrder->setStatus('sms_verified');
                    $verifyOrder->save();
                    $result = 1;
                }
                    
                else
                    $result = 0;
            }
            
        } else {
            $result = -1;
        }


        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($result);

        return $resultJson;        
        
    }
}