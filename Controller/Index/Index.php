<?php

/**
 * Package : Webworks
 * Module  : Sms
 * File    : Webworks/Sms/Controller/Index/Index.php
 * Date    : 28-04-2018
 * Copyright : Copyright (c) 2017 Webworks.pk
 * @Author  : Naveed Asim <naveed.asim@webworks.pk>
 * @Company : Webworks Solutions Pvt. Ltd. <webworks.pk>
 */
 
namespace Webworks\Sms\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Webworks\Sms\Model\SmsFactory;
 
class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;

    protected $_modelSmsFactory;
 
    public function __construct(Context $context, 
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        SmsFactory $modelSmsFactory)
    {
        $this->_resultPageFactory = $resultPageFactory;

        parent::__construct($context);

        $this->_modelSmsFactory = $modelSmsFactory;
    }
 
    public function execute()
    {
        
        $url = "http://bsms.its.com.pk/api.php?".
                "key=007a0a44cf61fb301e61297b0119aa14&".
                "receiver=923314318787&".
                "sender=Khaadi&".
                "msgdata=Testing&".
                "response_type=json";

        //Curl Start
        $ch  =  curl_init();
        $timeout  =  30;
        curl_setopt ($ch,CURLOPT_URL, $url) ;
        curl_setopt ($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch,CURLOPT_CONNECTTIMEOUT, $timeout) ;
        $response = curl_exec($ch) ;
        curl_close($ch);
    }
}